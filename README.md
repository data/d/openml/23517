# OpenML dataset: numerai28.6

https://www.openml.org/d/23517

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Numer.ai  
**Source**: [Kaggle](https://www.kaggle.com/numerai/encrypted-stock-market-data-from-numerai)  
**Please cite**:   

**Encrypted Stock Market Training Data from Numer.ai**  
The data is cleaned, regularized and encrypted global equity data. The first 21 columns (feature1 - feature21) are features, and target is the binary class you’re trying to predict.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/23517) of an [OpenML dataset](https://www.openml.org/d/23517). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/23517/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/23517/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/23517/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

